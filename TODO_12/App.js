import React, {useState} from 'react';
import { StyleSheet, FlatList, View, Button } from 'react-native';
import { AspirationItem } from './components/AspirationItem';
import AspirationInput from './components/AspirationInput';

export default function App() {
  const [courseAspiration, setCourseAspirations] = useState([]);
  const [isAddMode, setIsAddMode] = useState(false);

  const addAspirationHandler = aspirationTitle => {
    setCourseAspirations(currentAspiration => [
     ...currentAspiration,
     {key: Math.random().toString(), value: aspirationTitle}
     ])
     setIsAddMode(false)
 };
    const removeAspirationHandler = aspirationKey => {
    setCourseAspirations(currentAspiration => {
      return currentAspiration.filter((aspiration) => aspiration.key != aspirationKey)
    })
  }
    return (
    <View style={styles.screen}>
    <Button title='Add New Aspiration' onPress={() => setIsAddMode(true)} />
    <AspirationInput visible={isAddMode} onAddAspiration = {addAspirationHandler}/>
    <FlatList
     data = {courseAspiration}
     renderItem = {itemData => (
     <AspirationItem id={itemData.item.key} onDelete={removeAspirationHandler} title={itemData.item.value}/>
     )}/>
    </View>
 );
}

const styles = StyleSheet.create({
 screen: {
   padding: 50,
 }
});
