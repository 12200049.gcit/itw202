import React from "react";
import {Text, View} from 'react-native';
export default function TextComponent() {
    return (
        <View style={{ padding: 100}}>
            <Text style={{ height: 50, borderWidth: 2, padding: 10}}>
                Here's some Text!
            </Text>
        </View>
    );
}