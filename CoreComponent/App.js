import { StyleSheet, Text, View, ScrollView, Image, TextInput, Button } from 'react-native';
import ViewComponent from './component/viewcomponent';
import TextComponent from './component/Textcomponet';
import ImageComponent from './component/imagecomponent';

export default function App() {
  return (
    <ScrollView style={{margin: 20}}>
      <Text>Some Text</Text>
      <View>
        <Text>Some more Text</Text>
        <Image style={{height: 100, width: 100}}
        source={{ uri: 'https://picsum.photos/64/64'}}
        />
      </View>
      <TextInput defaultValue="YOu can type here"/>
      <Button onPress={() => {
        alert('YOu tapped the button!');
      }}
      title = "press Me"/>
      <View style={{height: 10}}></View>
    <ViewComponent></ViewComponent>
    <View style={{height: 10}}></View>
    <TextComponent/>
    </ScrollView>
    
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logoContain: {
    width: 200, 
    height: 100,
    resizeMode: 'contain'
  },
  logoStrertch: {
    width: 200, 
    height: 100,
    resizeMode: 'Stretch'
  }
});
