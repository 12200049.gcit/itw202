import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import Colors from '../../constants/colors';

function title ({children}){
  return (
   <Text style={styles.title}>{children}</Text>
  )
}

export default title;

const styles = StyleSheet.create({
    title: {
    fontFamily: 'open-sans-bold',
    fontSize: 24,
    // fontWeight:'bold',
    color: 'white',
    textAlign: 'center',
    borderWidth: 2,
    borderColor: 'white',
    padding: 12
    }
})