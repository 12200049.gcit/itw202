import { StyleSheet, Text, View,Image } from 'react-native'
import React from 'react'
import Title from '../components/ui/title'
import { Colors } from 'react-native/Libraries/NewAppScreen'
import Primarybutton from '../components/ui/Primarybutton'

function GameOverScreen({roundsNumber,userNumber, onStartNewGame}){
  return (
    <View style={styles.rootContainer}>
      <Title>GAME OVER!</Title>
      <View style={styles.imagecontainer}>
        <Image
          style={styles.image} 
          source={require('../assets/Image/success.png')}/>
      </View>
      <Text style={styles.summaryText}>
        Your phone needed <Text style={styles.highlight}>{roundsNumber} </Text>
        rounds to guess the number <Text style={styles.highlight}> {userNumber}</Text>
      </Text>
      <Primarybutton onPress={onStartNewGame}>Start New Game</Primarybutton>
    </View>
    
  )
}

export default GameOverScreen

const styles = StyleSheet.create({
  rootContainer: {
    flex: 1,
    padding: 24,
    alignItems: 'center',
    justifyContent: 'center',
  },
  imagecontainer: {
    width: 300,
    height: 300,
    borderRadius: 150,
    borderWidth: 3,
    borderColor: Colors.primary800,
    overflow: 'hidden',
    margin: 36,
  },
  image: {
    height: '100%',
    width: '100%',
  },
  summaryText: {
    fontFamily: 'open-sans',
    fontSize: 24,
    alignItems: 'center',
    marginBottom: 24
  },
  highlight: {
    fontFamily: 'open-sans-bold',
    color: Colors.primary500
  }
})