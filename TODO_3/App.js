import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import Courses from './component/funcComponent';

export default function App() {
  return (
    <View style={styles.container}>
     <Courses></Courses>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    padding: '10%',
    justifyContent: 'center',
  },
  text: {
    marginTop: '5%',
  }
});
