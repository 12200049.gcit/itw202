// import { StatusBar } from 'expo-status-bar';
// import { theme } from './src/core/theme';
// import Button from './src/components/Button';
// import CustomInput from './src/components/CustomInput';
// import Header from './src/components/Header';
// import Paragraph from './src/components/paragraph';
// import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
// import { createNativeStackNavigator } from '@react-navigation/native-stack';
// import { createDrawerNavigator } from '@react-navigation/drawer';

import {Provider } from 'react-native-paper';
import { NavigationContainer } from '@react-navigation/native';
import 'react-native-gesture-handler'
import Mystack from './navigation/Mystack';
import firebase from 'firebase/compat/app';
import {firebaseConfig} from './src/core/config';

if(!firebase.apps.length){
  firebase.initializeApp(firebaseConfig);
}

export default function App() {
  return (
    <Provider>
      <NavigationContainer>
        <Mystack/>
      </NavigationContainer>
    </Provider>     
  );
}

