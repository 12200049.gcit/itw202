import React from 'react'
import {View} from 'react-native'
import HomeScreen from '../src/Screens/HomeScreen';
import ProfileScreen from '../src/Screens/ProfileScreen';
import LoginScreen from '../src/Screens/LoginScreen'
import MyTab from './MyTab'
import { createDrawerNavigator } from '@react-navigation/drawer';
import 'react-native-gesture-handler'
import DrawerContent from './DrawerContent';

const Drawer = createDrawerNavigator();

export default function MyDrawer() {

  return (
      <Drawer.Navigator drawerContent={DrawerContent}>  
        <Drawer.Screen name='Home' component={MyTab}/>
        {/* <Drawer.Screen name='Profile' component={ProfileScreen}/>
        <Drawer.Screen name='Logout' component={LoginScreen}/>       */}
      </Drawer.Navigator>  
  )
}