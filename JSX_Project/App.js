import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import MyComponent from './component/Mycomponent';

export default function App() {
  return (
    <View style = {styles.container}>
      <MyComponent/>      
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  welcome: {
    flex: 1,
    fontSize: 24,
    color: 'F5FcFF',
  }
})


