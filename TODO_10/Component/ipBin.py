def convert(ip):
    print("IP Address : ", ip)
    binary = ''
    for dec in ip.split('.'):
        binary += bin(int(dec))[2:].rjust(8,'0')+" "
    return binary

ip_add = input("Enter IP Address: ")
print(convert(ip_add)) 