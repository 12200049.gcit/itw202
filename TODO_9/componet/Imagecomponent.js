import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';

const ImageComponent = () => {
  return (
    <View>
        <Image style={styles.logoContain}
        source={require('../assets/images.png')}/>
        <Image style={styles.logostretch}
        source={{uri: 'https://picum.photos/100/100'}}/>
    </View>
  );
}
export default ImageComponent;

const styles = StyleSheet.create({
  logoContain: {
    width: 100,
    height: 100,
    resizeMode: 'contain',
  },
  logostretch: {
    width: 100,
    height: 100,
    resizeMode: 'stretch',
  }
});
