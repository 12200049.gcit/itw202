import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import { add, multiply } from './component/nameExpo';
import division from './component/defualtExpo';

export default function App() {
  return (
    <View style={styles.container}>
      <Text>Open up App.js to start working on your app!</Text>
      <Text>Result of Addititon: {add(5, 6)}</Text>
      <Text>Result of Multiplication: {multiply(5, 8)}</Text>
      <Text>Result of division: {division(10, 2 )}</Text>
      <StatusBar style="auto" />
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
